package com.company;

public class InsufficientBalanceException extends Exception{

    private double amount;

    public double getBalance(){
        return amount;
    }

    public InsufficientBalanceException(double amount){

        this.amount=amount;
    }


}
