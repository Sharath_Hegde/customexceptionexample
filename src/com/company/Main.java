package com.company;

public class Main {

    public static void main(String[] args) {


        AccountOperations operations = new AccountOperations(1011);
        System.out.println("Depositing 25000Rs");
        operations.deposit(25000.00);
        try
        {
            System.out.println("\nWithdrawing 100Rs");
            operations.withdraw(100.00);
            System.out.println("\nWithdrawing 26600RS");
            operations.withdraw(26600.00);
        }catch(InsufficientBalanceException e)
        {
            System.out.println("Sorry, but you are short "
                    + e.getBalance());
            e.printStackTrace();
        }

    }
}
