package com.company;

public class AccountOperations {

    private double accountBalance;
    private int accountNumber;

    public AccountOperations(int number)
    {
        this.accountNumber = number;
    }
    public void deposit(double amount)
    {
        accountBalance += amount;
    }
    public void withdraw(double amount) throws InsufficientBalanceException{
        if(amount <= accountBalance)
        {
            accountBalance -= amount;
        }
        else
        {
            double needs = amount - accountBalance;
            throw new InsufficientBalanceException(needs);
        }
    }
    public double getBalance()
    {
        return accountBalance;
    }
    public int getNumber()
    {
        return accountNumber;
    }
}

